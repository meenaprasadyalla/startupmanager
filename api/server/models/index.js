const Sequelize = require('sequelize');

// Option 1: Passing parameters separately
// const sequelize = new Sequelize('database', 'username', 'password', {
//   host: 'localhost',
//   dialect:'postgres'
// });

// Option 2: Using a connection URI
const sequelize = new Sequelize('postgres://navajna:navajnasql@192.168.99.100:5436/facerecognition');
sequelize
  .authenticate()
  .then(() => {
    console.log('Connection has been established successfully.');
  })
  .catch(err => {
    console.error('Unable to connect to the database:', err);
  });


 let models = {};
// models.Certificate = sequelize.import('./certificate')
// models.Policy = sequelize.import('./policy')
// models.DeviceDetails = sequelize.import('./deviceDetails')
// models.Data = sequelize.import('./data')
// models.Feed = sequelize.import('./feed')
// models.Device = sequelize.import('./device')
// models.User = sequelize.import('./user')
// models.CoAPGateway = sequelize.import('./coap')
// models.License = sequelize.import('./license')
// models.DockerDevice = sequelize.import('./dockerDevice')
// models.DockerFeed = sequelize.import('./dockerFeed')
// models.DockerFeature = sequelize.import('./dockerFeature')
// models.DockerFeedData = sequelize.import('./dockerFeedData')
// models.DockerCertificate = sequelize.import('./dockerCertificate')
// models.DockerPolicy = sequelize.import('./dockerPolicy')
// models.DockerDeviceLog = sequelize.import('./dockerDeviceLog')
// models.FeatureLog = sequelize.import('./featureLog')
// models.mlData = sequelize.import('./mlData')
// models.FeatureVersion = sequelize.import('./featureVersion')
// models.PrototypeLicense = sequelize.import('./prototype_license')
// models.PrototypeBilling = sequelize.import('./prototype_billing')
// models.PrototypePayment = sequelize.import('./prototype_payment')
// models.PrototypeUsagePlan = sequelize.import('./prototype_usage_plan')




// /*models.Policy.sync({});
// models.DeviceDetails.sync({});
// models.Data.sync({});
// models.Feed.sync({});
// models.Device.sync({});
// models.User.sync({});
// models.CoAPGateway.sync({});
// models.License.sync({});
// models.DockerDevice.sync({});
// models.DockerFeed.sync({});
// models.DockerFeature.sync({force : true});
// models.DockerFeedData.sync({});
// models.DockerCertificate.sync({});
// models.DockerPolicy.sync({});
// models.DockerDeviceLog.sync({force : true});
// models.FeatureLog.sync({});
// models.mlData.sync({});
// models.FeatureVersion.sync({}); 
// models.PrototypeUsagePlan.sync({}); 
// models.PrototypeLicense.sync({}); 
// models.PrototypeBilling.sync({}); 
// models.PrototypePayment.sync({}); */
// models.PrototypeUsagePlan.sync({})


// Object.keys(models).forEach(modelName => {
//     models.Certificate.sync({});
//     if ('associate' in models[modelName]) {
//         models[modelName].associate(models)
//     }
// })

 models.sequelize = sequelize
 models.Sequelize = Sequelize

// module.exports = models;
module.exports = models;
