'use strict'
const config		=	require("./config/config.development");
var express			=	require('express');
var bodyParser		=	require('body-parser');
var path			=	require('path');
var cors			=	require('cors');
const app			=	express();
const fileUpload = require('express-fileupload');
var isProd = (config.env === 'production');
app.use(cors({origin: '*'}));
app.set('port', process.env.PORT || '7000')
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: false }))
//app.use(fileUpload());
app.use(fileUpload({
    limits: { fileSize: 10000 * 1024 },
  }));
//   app.use(function(req, res, next) {
//     res.header('Access-Control-Allow-Origin', '*');
//     res.header('Access-Control-Allow-Methods', 'GET, PUT, POST, DELETE, OPTIONS');
//     res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization, Content-Length, X-Requested-With');
//     if ('OPTIONS' === req.method) {
//       res.send(200);
//     }
//     else {
//       next();
//     }
//  });
 app.use('/', require('./server/routes'))

 app.use(errorHandler);
 function errorHandler(err, req, res, next) {
  if (!isProd)
      console.log(err);
  res.status(err.status || 500)
  res.json({
      success: false,
      message: err.message,
      error: isProd ? err.name : err.stack
  })
}
app.listen(app.get('port'), () => {
  console.log('Listening on port ' + app.get('port'))
})

module.exports = app;